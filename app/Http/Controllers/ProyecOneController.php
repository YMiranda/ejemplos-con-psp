<?php

namespace App\Http\Controllers;

use App\ProyecOne;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;


class ProyecOneController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('uno');
    }

    /**
     * @param Request $request
     * @return Factory|View
     * @throws ValidationException
     */
    public function post(Request $request){
        $this->validate($request, [
            'lista' => 'required',
        ]);
        $lista =explode(',', $request->get('lista'));
        $calculos = new ProyecOne();
        $desviacion = $calculos->desviacionEstandar($lista);
        $media = $calculos->media($lista);

        return view('uno')->with([
            'desviacion' => $desviacion,
            'media' => $media
        ]);
    }
}
