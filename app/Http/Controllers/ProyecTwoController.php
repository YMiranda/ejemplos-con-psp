<?php

namespace App\Http\Controllers;

use App\ProyecTwo;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class ProyecTwoController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index(){
        return view('loc');
    }

    /**
     * @param Request $request
     * @return Factory|View
     * @throws ValidationException
     */
    public function post(Request $request){

        $this->validate($request, [
            'files' => 'required',
        ]);
        $archivos = collect();
        if ($request->hasFile('files')){
            foreach ($request->file('files') as $k => $file){
                if ($file->getClientOriginalExtension() === 'php'){
                    $proceso = new ProyecTwo($file);
                    $proceso->procesar();
                    $datos = [
                        'nombre' => $file->getClientOriginalName(),
                        'funciones' => $proceso->getFunciones(),
                        'fisicas' => $proceso->getFisicas(),
                        'clases' => $proceso->getClases(),
                        'vacias' => $proceso->getVacias()
                    ];
                    $archivos->push( $datos );
                }
            }
        }
        return view('loc')->with([
            'data' => $archivos
        ]);
    }
}
