<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class WelcomeController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index(){
        return view('welcome');
    }
}
