<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class ProyecThreeController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('tres');
    }

    /**
     * @param Request $request
     * @return Factory|View
     * @throws ValidationException
     */
    public function post(Request $request){
        $this->validate($request, [
            'lista' => 'required',
        ]);


        return view('tres')->with([
            'desviacion' => '',
        ]);
    }
}
