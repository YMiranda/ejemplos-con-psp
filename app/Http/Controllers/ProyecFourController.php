<?php

namespace App\Http\Controllers;

use App\ProyecFour;
use Illuminate\Http\Request;

class ProyecFourController extends Controller
{
    public function index(){
        return view('cuatro');
    }

    public function post(Request $request){
        $this->validate($request, [
            'datos' => 'required',
        ]);
        $data = explode("\r\n", $request->datos);
        $da = array();
        foreach ($data as $key => $item){
            $d = explode(',',$item);
            $da[0][$key] =(float) $d[0];
            if (array_key_exists(1,$d)){
                $da[1][$key] =(float) $d[1];
            }
        }
        if(array_key_exists(1,$da)){
            $resultados[0] = ProyecFour::metodoLOC($da[0],$da[1]);
        }else{
            $resultados = $da;
        }
        $resultados = new ProyecFour($resultados[0]);
        $resultados->resultados();
        return view('cuatro')->with([
            'data' => $resultados
        ]);
    }
}
