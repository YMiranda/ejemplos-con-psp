<?php

namespace App;


class ProyecFour
{
    private $array;

    private $VS, $S, $M, $L, $VL;
    private const DECIMALES = 4;

    public function __construct($array)
    {
        $this->array = $array;
    }

    private function avg(): float
    {
        return array_sum($this->logaritmoN($this->array)) / count($this->array);
    }

    private function varianza(): float
    {
        $avg = $this->avg();
        $sum = $this->logaritmoN();
        foreach ($sum as $key => $item){
            $sum[$key] = ($item-$avg)**2;
        }
        $numerador = array_sum($sum);
        $denominador = count($sum)-1;
        return $numerador/$denominador;
    }

    private function desviacionEstandar(): float
    {
        return sqrt($this->varianza());
    }

    private function logaritmoN(): array
    {
        $array = $this->array;
        foreach ($array as $key => $item){
            $array[$key] = log($item);
        }
        return $array;
    }

    private function rangoLogaritmo(): void
    {
        $avg = $this->avg();
        $desviacion = $this->desviacionEstandar();
        $this->VS = $avg - (2*$desviacion);
        $this->S = $avg - $desviacion;
        $this->M = $avg;
        $this->L = $avg + $desviacion;
        $this->VL = $avg + (2*$desviacion);
    }

    public function resultados(): void
    {
        $this->rangoLogaritmo();
        $this->VS = exp($this->VS);
        $this->S = exp($this->S);
        $this->M = exp($this->M);
        $this->L = exp($this->L);
        $this->VL = exp($this->VL);
    }

    /**
     * @return mixed
     */
    public function getVS()
    {
        return number_format($this->VS,self::DECIMALES);
    }

    /**
     * @return mixed
     */
    public function getS()
    {
        return number_format($this->S,self::DECIMALES);
    }

    /**
     * @return mixed
     */
    public function getM()
    {
        return number_format($this->M,self::DECIMALES);
    }

    /**
     * @return mixed
     */
    public function getL()
    {
        return number_format($this->L,self::DECIMALES);
    }

    /**
     * @return mixed
     */
    public function getVL()
    {
        return number_format($this->VL,self::DECIMALES);
    }

    /**
     * @param $array1
     * @param $array2
     * @return mixed
     */
    public static function metodoLOC($array1, $array2){
        foreach ($array1 as $key => $value){
            $array1[$key] = $value / $array2[$key];
        }
        return $array1;
    }
}