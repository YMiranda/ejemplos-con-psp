<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyecOne extends Model
{

    /**
     * @param $lista
     * @return string
     */
    public function desviacionEstandar($lista): string
    {
        return number_format( $this->standard_deviation($lista, true) , 2);
    }

    /**
     * @param $list
     * @return string
     */
    public function media($list): string
    {
        $sum=0;
        foreach ($list as $num){
            $sum+=$num;
        }
        return number_format($sum/count($list), 2);
    }

    /**
     * @param $aValues
     * @param bool $bSample
     * @return float
     */
    private function standard_deviation($aValues, $bSample = false): float
    {
        $fMean = array_sum($aValues) / count($aValues);
        $fVariance = 0.0;
        foreach ($aValues as $i)
        {
            $fVariance += ($i - $fMean) ** 2;
        }
        $fVariance /= ( $bSample ? count($aValues) - 1 : count($aValues) );
        return (float) sqrt($fVariance);
    }
}
