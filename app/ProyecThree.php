<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyecThree
{

    private $x;
    private $y;
    private $count;

    private $beta0;
    private $beta1;
    private $correlacion;

    /**
     * ProyecThree constructor.
     * @param $x
     * @param $y
     */
    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
        if (count($x) === count($y)){
            $this->count = count($x);
        }
    }

    public function regresionLineal() {
        $x = $this->x;
        $y = $this->y;
        $numerador = $this->multiplicado($x, $y)-($this->count*($this->avg($x)*$this->avg($y)));
        $denominador = $this->cuadrado($x)-($this->count*($this->avg($x)**2));
        $this->beta1 = $numerador/$denominador;
        $this->beta0 = $this->avg($y) - ($this->beta1*$this->avg($x));
    }

    function correlacion(){
        $x = $this->x;
        $y = $this->y;
        $numerador = ($this->count*$this->multiplicado($x, $y))-($this->avg($x)*$this->avg($y));
//        $denominador = ;
    }

    private function avg($array){
        return array_sum($array)/count($array);
    }

    public function cuadrado($array){
        $resultado = array();
        foreach ($array as $key => $value){
            $resultado[$key] = $value  * $value;
        }
        return array_sum($resultado);
    }

    public function multiplicado($array1, $array2){
        $array3 = array();
        foreach ($array1 as $key=>$value){
            $array3[$key] = $value * $array2[$key];
        }
        return array_sum($array3);
    }

    /**
     * @return mixed
     */
    public function getBeta0()
    {
        return $this->beta0;
    }

    /**
     * @return mixed
     */
    public function getBeta1()
    {
        return $this->beta1;
    }

    /**
     * @return mixed
     */
    public function getCorrelacion()
    {
        return $this->correlacion;
    }
}
