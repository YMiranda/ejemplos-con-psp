<?php

namespace App;


use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class ProyecTwo
{
    private $file;
    private $vacias;
    private $funciones;
    private $clases;
    private $fisicas;

    /**
     * ProyecTwo constructor.
     * @param $file
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     *
     */
    public function procesar(){
        $file = File::get($this->file);
        $archivo = Collection::make(explode("\n", $file));
        $this->fisicas = $archivo->count();
        foreach ($archivo as $line){
            $line = preg_replace('/\s+/', ' ', $line);
            if (empty($line) || $line === ''){
                $this->vacias++;
            }
            if ( Str::contains($line, 'class')) {
                $this->clases++;
            }
            if ( Str::contains($line, 'function')) {
                $this->funciones++;
            }
        }
    }

    /**
     * @return mixed
     */
    public function getVacias()
    {
        return $this->vacias;
    }

    /**
     * @return mixed
     */
    public function getFunciones()
    {
        return $this->funciones;
    }

    /**
     * @return mixed
     */
    public function getClases()
    {
        return $this->clases;
    }

    /**
     * @return mixed
     */
    public function getFisicas()
    {
        return $this->fisicas;
    }
}
