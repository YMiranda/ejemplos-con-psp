@extends('app')
@section('title', 'Proyecto Cuatro')
@section('content')
    <div class="title m-b-md">
        Curso PSP
    </div>
    <p>Luis Yair Miranda Gonzalez</p>
    <div class="links">
        <a href="{{ route('uno') }}">Proyecto Uno</a>
        <a href="{{ route('loc') }}">Proyecto Dos</a>
        <a href="{{ route('tres') }}">Proyecto Tres</a>
        <a href="{{ route('cuatro') }}">Proyecto Cuatro</a>
    </div>
@endsection