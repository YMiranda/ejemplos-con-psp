@extends('app')
@section('title', 'Proyecto Uno')
@section('content')
    <form method="POST" action="{{ action('ProyecOneController@post') }}">
        @csrf
        <div class="form-group">
            <label for=lista""> Lista de numeros:
                <textarea class="form-control" id="lista" rows="4" cols="50" placeholder="Agregue los elementos separados por comas"
                          name="lista"></textarea>
        </div>
        <button type="submit" class="btn btn-primary mb-2">Calcular</button>
    </form>
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Se detectaron los siguiente errores.<br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @isset($desviacion)
        <p>La desviacion es: {{ $desviacion }}</p>
        <p>La media es: {{ $media }}</p>
    @endisset
@endsection