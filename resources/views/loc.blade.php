@extends('app')
@section('title', 'Proyecto Dos')
@section('content')
    <form method="POST" action="{{ action('ProyecTwoController@post') }}" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="uploadFiles">Directorio: </label>
            <input class="form-control-file" type="file" name="files[]" id="uploadFiles" aria-describedby="filesHelp"
                   placeholder="Archivos de proyecto" multiple directory webkitdirectory mozdirectory>
        </div>
        <button type="submit" class="btn btn-primary mb-2">Cargar</button>
    </form>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Se detectaron los siguiente errores.<br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @isset($data)
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Lineas Fisicas</th>
                <th scope="col">Funciones</th>
                <th scope="col">Clases</th>
                <th scope="col">Lineas Vacias</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data as $key => $item)
                <tr>
                    <th scope="row">{{ $key+1 }}</th>
                    <td>{{ $item['nombre'] }}</td>
                    <td>{{ $item['fisicas'] }}</td>
                    <td>{{ $item['funciones'] }}</td>
                    <td>{{ $item['clases'] }}</td>
                    <td>{{ $item['vacias'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endisset
@endsection