@extends('app')
@section('title', 'Proyecto Cuatro')
@section('content')
    <form method="POST" action="{{ action('ProyecFourController@post') }}">
        @csrf
        <div class="form-group">
            <label for=datos"">Elementos:</label>
                <textarea class="form-control" id="datos" rows="4" cols="50"
                          placeholder="Separe las columnas por comas y las filas por saltos de linea"
                          name="datos"></textarea>
        </div>
        <button type="submit" class="btn btn-primary mb-2">Procesar</button>
    </form>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Se detectaron los siguiente errores.<br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @isset($data)
        <table class="table">
            <thead>
            <tr>
                <th scope="col">VS</th>
                <th scope="col">S</th>
                <th scope="col">M</th>
                <th scope="col">L</th>
                <th scope="col">VL</th>
            </tr>
            </thead>
            <tbody>
{{--            @foreach ($data as $key => $item)--}}
                <tr>
                    <td>{{ $data->getVS() }}</td>
                    <td>{{ $data->getS() }}</td>
                    <td>{{ $data->getM() }}</td>
                    <td>{{ $data->getL() }}</td>
                    <td>{{ $data->getVL() }}</td>
                </tr>
{{--            @endforeach--}}
            </tbody>
        </table>
    @endisset
@endsection