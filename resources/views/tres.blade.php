@extends('app')
@section('title', 'Proyecto Tres')
@section('content')
    <form method="POST" action="{{ action('ProyecThreeController@post') }}" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="uploadFiles">Directorio: </label>
            <input class="form-control-file" type="file" name="files[]" id="uploadFiles" aria-describedby="filesHelp"
                   placeholder="Archivos de proyecto" multiple directory webkitdirectory mozdirectory>
        </div>
        <button type="submit" class="btn btn-primary mb-2">Cargar</button>
    </form>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Se detectaron los siguiente errores.<br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @isset($data)
        {{ dd($data) }}
    @endisset
@endsection