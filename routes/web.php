<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index')->name('welcome');

Route::get('/uno', 'ProyecOneController@index')->name('uno');
Route::post('/uno', 'ProyecOneController@post');

Route::get('/loc', 'ProyecTwoController@index')->name('loc');
Route::post('/loc', 'ProyecTwoController@post');

Route::get('/tres', 'ProyecThreeController@index')->name('tres');
Route::post('/tres', 'ProyecThreeController@post');

Route::get('/cuatro', 'ProyecFourController@index')->name('cuatro');
Route::post('/cuatro', 'ProyecFourController@post');
