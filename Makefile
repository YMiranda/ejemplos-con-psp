init:
	php composer.phar install
	@if [ ! -f .env ];\
		then cp .env.example .env;\
		echo "Copied from .env.example";\
		php artisan key:generate;\
	fi
	npm install
	npm run dev
	php artisan serve
serve:
	php artisan serve